package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreadControllerTests {
    private User loggedIn;
    private Forum toCreate;
    private ThreadController controller;
    private Thread thread;

    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        thread = new Thread(
                "some",
                new Timestamp(System.currentTimeMillis()),
                "forum",
                "message",
                "slug",
                "title",
                5
                );
        thread.setId(1);
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        controller = new ThreadController();
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatesPosts() {
        long time = System.currentTimeMillis();
        List<Post> toCreate = List.of(new Post(
                "some",
                new Timestamp(time),
                "",
                "message",
                0,
                0,
                false
        ));
        List<Post> created = List.of(new Post(
                "some",
                new Timestamp(time),
                "forum",
                "message",
                0,
                1,
                false
        ));

        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic<ThreadDAO> thMock = Mockito.mockStatic(ThreadDAO.class)) {
                thMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                        .thenReturn(thread);
                ResponseEntity result = controller.createPost("slug", toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(created), result, "Result for succeeding posts creation");
            }
        }
    }

    @Test
    @DisplayName("Correct posts retrieval test")
    void correctlyGetPosts() {
        long time = System.currentTimeMillis();
        List<Post> posts = List.of(new Post(
                "some",
                new Timestamp(time),
                "forum",
                "message",
                0,
                1,
                false
        ));

        try (MockedStatic<ThreadDAO> thMock = Mockito.mockStatic(ThreadDAO.class)) {
            thMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(thread);
            thMock.when(() -> ThreadDAO.getPosts(1, 1, 0, "", false))
                    .thenReturn(posts);
            ResponseEntity result = controller.Posts("slug", 1, 0, "", false);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), result, "Result for succeeding posts retrieval");
        }
    }

    @Test
    @DisplayName("Correct thread change test")
    void correctlyChangeThread() {
        Thread changed = new Thread(
                "some",
                new Timestamp(System.currentTimeMillis()),
                "forum",
                "message",
                "slug",
                "newTitle",
                5
        );
        try (MockedStatic<ThreadDAO> thMock = Mockito.mockStatic(ThreadDAO.class)) {
            thMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(thread);
            thMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(changed);
            ResponseEntity result = controller.change("slug", changed);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(changed), result, "Result for succeeding thread modification");
        }
    }

    @Test
    @DisplayName("Correct thread retrieval test")
    void correctlyGetThread() {
        try (MockedStatic<ThreadDAO> thMock = Mockito.mockStatic(ThreadDAO.class)) {
            thMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(thread);
            ResponseEntity result = controller.info("slug");
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), result, "Result for succeeding thread retrieval");
        }
    }

    @Test
    @DisplayName("Correct vote creation test")
    void correctlyCreateVote() {
        Vote toCreate = new Vote("some", 1);
        Thread changed = new Thread(
                "some",
                thread.getCreated(),
                "forum",
                "message",
                "slug",
                "title",
                6
        );
        changed.setId(1);
        try (MockedStatic<ThreadDAO> thMock = Mockito.mockStatic(ThreadDAO.class)) {
            thMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(thread);
            thMock.when(() -> ThreadDAO.change(toCreate, 6))
                    .thenReturn(6);
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info("some"))
                        .thenReturn(loggedIn);
                ResponseEntity result = controller.createVote("slug", toCreate);
                assertEquals((int) toCreate.getTid(), 1);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(changed), result, "Result for succeeding thread retrieval");
            }
        }
    }
}

